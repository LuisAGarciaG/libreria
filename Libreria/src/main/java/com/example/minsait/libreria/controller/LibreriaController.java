package com.example.minsait.libreria.controller;

//import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.example.minsait.libreria.entity.LibreriaEntity;

import lombok.extern.slf4j.Slf4j;


@RestController
@Slf4j
public class LibreriaController {
	
	private RestTemplate restTemplate;

	@Autowired
	public void Controller(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
	
	@GetMapping("/books/list")
	public Object getBooks() {
		String url= "https://9ce6-2806-2a0-400-8a4e-4803-f374-6ffa-9b48.ngrok.io/libreria/books/";
		Object forObject = restTemplate.getForObject(url, Object.class);
		log.info("Result " + forObject);
		return forObject;
	}
	
	@GetMapping("/books/list/{id}")
	public Object getBookByBook(@PathVariable("id") String id) {
		String url= "https://9ce6-2806-2a0-400-8a4e-4803-f374-6ffa-9b48.ngrok.io/libreria/books/".concat(id);
		Object forObject = restTemplate.getForObject(url, Object.class);
		log.info("Result " + forObject);
		return forObject;   
	}
	
	
	@PutMapping("/books/list/{id}")
	public LibreriaEntity updateBook(@PathVariable("id") String id, @RequestBody LibreriaEntity libreriaEntity) {
		log.info("Prueba de actualización");
		return libreriaEntity.updateBook(id, libreriaEntity);
	}	
	
}
