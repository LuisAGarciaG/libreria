package com.example.minsait.libreria.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.minsait.libreria.entity.LibreriaEntity;


@Service
public class LibreriaService {
	
	private RestTemplate restTemplate;

	public LibreriaEntity updateBook(Long id, LibreriaEntity libreriaEntity) {
		Map<String, String > params = new HashMap<>();
		params.put("id", id.toString());
			restTemplate.put("https://9ce6-2806-2a0-400-8a4e-4803-f374-6ffa-9b48.ngrok.io/libreria/books/{id}", libreriaEntity, params);
			return libreriaEntity;
		}
	
}
